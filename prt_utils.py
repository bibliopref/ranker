def _merge_sort(l, comparer):
    """
    A merge sort with a user-provided comparer function (in the style of
    sorted(..., key=cmp_to_key(comparer)).
    """
    if len(l) == 1:
        # Nothing to do.
        return

    center = len(l) // 2
    left = l[:center]
    right = l[center:]

    # Recurse:
    _merge_sort(left, comparer)
    _merge_sort(right, comparer)

    # Cursors on left and on right:
    cl = cr = 0
    
    while cl < len(left) and cr < len(right):
        if comparer(left[cl], right[cr]) <= 0:
            l[cl+cr] = left[cl]
            cl += 1
        else:
            l[cl+cr] = right[cr]
            cr += 1

    if cl == len(left):
        # We exhausted left
        l[cl+cr:] = right[cr:]
    else:
        # We exhausted right
        l[cl+cr:] = left[cl:]


def mergesorted(iterable, comparer, reverse=False):
    """
    Similar to sorted(), but uses mergesort (which is guaranteed to never
    compare any two given items twice).
    """
    l = list(iterable)
    _merge_sort(l, comparer)
    return l
