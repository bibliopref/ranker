from functools import cmp_to_key
import random

from otree.api import *
from .prt_utils import mergesorted

doc = """
Based on http://sortablejs.github.io/Sortable/
"""

MENUS = {
         'journals' : ['Econometrica', 'The Quarterly Journal of Economics',
                       'American Economic Review',
                       'The Review of Economic Studies',
                       'Journal of Political Economy',
                       'American Economic Journal: Macroeconomics',
                       'Journal of Financial Economics',
                       'Journal of Economic Perspectives',
                       'The Review of Financial Studies',
                       'Journal of Finance'],
         'cities' : ['Rome', 'Washington D.C.', 'La Paz', 'Shanghai', 'Oslo',
                     'Tunisi', 'New Dehli', 'Lima', 'Madrid', 'Paris'],
         'numbers' : ['one', 'two', 'three', 'four', 'five',
                      'six', 'seven', 'eight', 'nine', 'ten'],
         'colors' : ['red', 'black', 'white', 'blue', 'purple',
                     'brown', 'green', 'yellow', 'orange', 'pink'],         
         }


class C(BaseConstants):
    NAME_IN_URL = 'rank_widget'
    PLAYERS_PER_GROUP = None
    # Worst case rounds:
    # from math import log2, ceil; sum([ceil(log2(i)) for i in range(1, n+1)])
    # -> 25 for n = 10 options (or https://oeis.org/A001855 )
    NUM_ROUNDS = 25


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


MODE_LABEL = ("What procedure do you want to test? (\"Tournament\": "
              "iteratively compare pairs of options; \"Insertion\": "
              "iteratively insert options; \"Rank\": just one-shot ranking; "
              "\"Fill\": like \"Rank\" but starting from an empty ranking.")

class Player(BasePlayer):
    ranking = models.StringField(blank=True)
    mode = models.StringField(choices=['Tournament',
                                       'Insertion',
                                       'Rank',
                                       'Fill'],
                              label=MODE_LABEL)
    items_type = models.StringField(choices=list(MENUS),
                                     label="What would you like to rank?")
    menu_length = models.IntegerField(choices=range(2, 11),
                                      label="How many options?")


    # Used to remember the options proposed round by round:
    option_A = models.IntegerField()
    option_B = models.IntegerField()

    # Used
    # - in "Tournament" for the actual binary choice, possible values 1 and -1
    # - in "Insertion" for the position indicated, possible values 1 to N
    comparison_choice = models.IntegerField(choices=range(-1, 11))

    # Just to persist to database the result of "Tournament.is_displayed()" or
    # "Insertion.is_displayed()":
    iteration_is_displayed = models.BooleanField()


def ranking_error_message(player, value):
    # FIXME: the destination box should not be emptied.
    options = player.menu_length
    if not value.count(',') == options - 1:
        return f"Please rank all {options} options"

PARAMS = ['mode', 'items_type', 'menu_length']

class Configure(Page):
    form_model = 'player'
    form_fields = PARAMS

    def is_displayed(player):
        if player.round_number == 1:
            return True
        else:
            for field in PARAMS:
                # These fields have constant values over time:
                setattr(player, field,
                        getattr(player.in_round(1), field))

    def before_next_page(player, timeout_happened):
        menu = MENUS[player.items_type][:player.menu_length]
        # FIXME: make this deterministic (based on journal list), so that if a
        # tournament is interrupted it can restart from where it left,
        # consistently:
        random.shuffle(menu)

        player.participant.vars['menu'] = menu

class UnknownComparison(Exception):
    """
    Internal way to signal what we still need to rank, in Tournament.
    """
    pass

class Tournament(Page):
    form_model = 'player'
    form_fields = ['comparison_choice']

    @staticmethod
    def is_displayed(player):
        """
        This function contains the "tournament ranking" logic - although the
        actual sorting logic is delegated to Python's "sorted".
        Note that the current version starts the sorting from scratch at each
        round. This is obviously inefficient, but the clearer code flow
        (we do not need to split the sorting algorithm across rounds) is
        probably worth the added complexity (of less than N*log(N) per round,
        N~10).
        Note that options are always referred to with numbers internally,
        including in the database, and that these numbers are initially
        ordered: the translation to string labels is only done (and randomized)
        in the html interface.
        """

        if player.mode != 'Tournament':
            return False

        # Just a stupid cachking mechanism to avoid the function being run
        # multiple times (as oTree unfortunately does, presumably in separate
        # threads):
        if player.field_maybe_none('iteration_is_displayed'):
            return player.iteration_is_displayed

        if 'order' in player.participant.vars:
            # Order already completed
            return False

        known_comparisons = (p.comparison_choice
                             for p in player.in_previous_rounds())

        def comparer(item1, item2):
            """
            This function ranks item1 and item2 based on previous comparisons,
            and if this is a new comparison raises an error; upon this error,
            the new comparison is set as to be proposed to the user.
            """
            try:
                known = next(known_comparisons)
                return known
            except StopIteration:
                player.option_A = item1
                player.option_B = item2
                raise UnknownComparison

        try:
            order = mergesorted(list(range(player.menu_length)), comparer)
        except UnknownComparison:
            # Let's show the page to propose this comparison:
            player.iteration_is_displayed = True
            return True

        # We just completed the ranking in the previous step, save the
        # resulting order...
        player.participant.vars['order'] = order
        # ... and skip this page from now on:
        player.iteration_is_displayed = False
        return False

    def vars_for_template(player):
        menu = player.participant.vars['menu']

        return {'label_A' : menu[player.option_A],
                'label_B' : menu[player.option_B]}

def _compute_insertion_order(player, include_current=False):
    last_round = player.round_number - 1 + include_current

    insertion_points = (player.in_round(r).comparison_choice
                        for r in range(1, last_round+1))

    order = [0]
    for idx, p in enumerate(insertion_points):
        order.insert(p, idx+1)
    return order

class Insertion(Page):
    form_model = 'player'
    form_fields = ['comparison_choice']

    @staticmethod
    def is_displayed(player):

        return (player.mode == 'Insertion' and
                # We need N-1 rounds to sort N options:
                player.round_number < player.menu_length)

    def vars_for_template(player):
        """
        This function calls the "insertion ranking" logic at each round.
        Note that the current version starts the sorting from scratch at each
        round. This is obviously inefficient, but guarantees consistence if the
        process is interrupted and restarted based only on the database
        content.
        Note that options are always referred to with numbers internally,
        including in the database, and that these numbers are initially
        ordered: the translation to string labels is only done (and randomized)
        in the html interface.
        """
        order = _compute_insertion_order(player)

        menu = player.participant.vars['menu']
        placed = [menu[pos] for pos in order]


        return {'options' : list(zip(range(1, len(placed)+1),
                                     # Templates can't do "+1"...
                                     range(2, len(placed)+2),
                                     placed)
                                ),
                'new' : menu[player.round_number]}

    def before_next_page(player, timeout_happened):
        """
        This function calls the "insertion ranking" logic only at the last
        round.
        """
        if player.round_number == player.menu_length - 1:
            # We just completed the ranking in the previous step, save the
            # resulting order (in reversed order, for consistency)...
            order = _compute_insertion_order(player, True)[::-1]
            player.participant.vars['order'] = order


class Rank(Page):
    form_model = 'player'
    form_fields = ['ranking']

    @staticmethod
    def vars_for_template(player):
        menu = player.participant.vars['menu']
        if player.mode in ['Tournament', 'Insertion']:
            order = player.participant.vars['order']
        else:
            order = range(len(menu))
        return {'options' : [(i, menu[i]) for i in order[::-1]],
                # This is just set as default value - returned if the user
                # makes no change:
                'initial' : ','.join([str(i) for i in order[::-1]]),
                'random_start' : player.mode in ['Rank', 'Fill']}

    def is_displayed(player):
        return player.round_number == C.NUM_ROUNDS


class Results(Page):
    form_model = 'player'
    def is_displayed(player):
        return player.round_number == C.NUM_ROUNDS

    def vars_for_template(player):
        menu = player.participant.vars['menu']
        idces = [int(i) for i in player.ranking.split(',')]
        result = ", ".join([menu[i] for i in idces])
        return {"ranking" : result}

page_sequence = [Configure, Tournament, Insertion, Rank, Results]
